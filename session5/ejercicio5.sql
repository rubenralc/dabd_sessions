--INICIAL
select p.UsuariId, p.LlibreId, p.TempsId, p.dies
from Prestecs p;

--A:=Roll-up(Prestecs, Temps.mes, Sum)
select p.UsuariId, p.LlibreId, t.mes, sum(p.dies) as dies
from Prestecs p, Temps t
where p.TempsId = t.Id
group by p.UsuariId, p.LlibreId, t.mes;

--B:=Selecció(A, Temps.mes='Gener')
select p.UsuariId, p.LlibreId, t.mes, sum(p.dies) as dies
from Prestecs p, Temps t
where p.TempsId = t.Id
      and t.mes = 'Gener'
group by p.UsuariId, p.LlibreId, t.mes;

--R:=Drill-down(B, Temps.Id)
select p.UsuariId, p.LlibreId, t.mes, sum(p.dies) as dies
from Prestecs p, Temps t
where p.TempsId = t.Id
      and t.mes = 'Gener'
group by p.UsuariId, p.LlibreId, t.mes;