--INICIAL:
select c.ProveidorId, c.LlibreId, c.TempsId, c.preu, c.numUnitats
from Compres c;

--A:=Roll-up(Compres,Proveidors.All,Sum)
select 'All' as ProveidorId, c.LlibreId, c.TempsId, sum(c.preu) as preu, sum(c.numUnitats) as numUnitats
from Compres c
group by c.LlibreId, c.TempsId;

--B:=Roll-up(A, Llibres.All,Sum)
select 'All' as ProveidorId, 'All' as LlibreId, c.TempsId, sum(c.preu) as preu, sum(c.numUnitats) as numUnitats
from Compres c
group by c.TempsId;

--C:=Roll-up(B, Temps.All,Sum)
select 'All' as ProveidorId, 'All' as LlibreId, 'All' as TempsId, sum(c.preu) as preu, sum(c.numUnitats) as numUnitats
from Compres c;

--D:=CanviBase(C, Proveidors)
select 'All' as LlibreId, 'All' as TempsId, sum(c.preu) as preu, sum(c.numUnitats) as numUnitats
from Compres c;


--R:=Projecció(D, numUnitats)
select 'All' as LlibreId, 'All' as TempsId, sum(c.numUnitats) as numUnitats
from Compres c;

--FINAL:
create view nomVista as
select 'All' as LlibreId, 'All' as TempsId, sum(c.numUnitats) as numUnitats
from Compres c;
