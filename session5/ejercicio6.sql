CREATE VIEW nomVista AS (
    SELECT d.poblacioID, p.provincia, d.rangID, AVG(d.mossos) AS Promig
    FROM destinacio d INNER JOIN poblacio p ON d.poblacioID = p.nom
                      INNER JOIN data data ON d.dataID = data.id
    WHERE data.anyo = '2003' AND
          d.poblacioID IN ('Badalona', 'Hospitalet', 'Lleida', 'Ponts') AND
          d.especialitatID = 'Transit' AND
          d.rangID IN ('Caporal', 'Mosso')
    GROUP BY ROLLUP (d.rangID, p.provincia, d.poblacioID)
);