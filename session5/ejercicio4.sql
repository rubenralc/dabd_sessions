--INICIAL:
select ProveidorId, LlibreId, TempsId, preu, numUnitats
from Compres;

--A:=Roll-up(Compres, Proveidors.All)
select 'All' as ProveidorId, LlibreId, TempsId, sum(preu) as preu, sum(numUnitats) as numUnitats
from Compres
group by LlibreId, TempsId;

--B:=CanviBase(A, Llibres x Temps)
select LlibreId, TempsId, sum(preu) as preu, sum(numUnitats) as numUnitats
from Compres
group by LlibreId, TempsId;

--C:=Drill-across(B, Prestecs, Sum)
select c.LlibreId, c.TempsId, sum(c.preu) as preu, sum(c.numUnitats) as numUnitats, sum(p.dies) as dies
from Compres c, Prestecs p
where c.LlibreId = p.LLibreId and c.TempsId = p.TempsId
group by c.LlibreId, c.TempsId;

--D:=Projeccio(C, dies)
select c.LlibreId, c.TempsId, sum(p.dies) as dies
from Compres c, Prestecs p
where c.LlibreId = p.LLibreId and c.TempsId = p.TempsId
group by c.LlibreId, c.TempsId;

--R:=CanviBase(D, Llibres x Temps x Usuaris)
select c.LlibreId, c.TempsId, sum(p.dies) as dies
from Compres c, Prestecs p
where c.LlibreId = p.LLibreId and c.TempsId = p.TempsId
group by c.LlibreId, c.TempsId;

--FINAL:
create view nomVista as
select c.LlibreId, c.TempsId, p.UsuariId, sum(p.dies) as dies
from Compres c, Prestecs p
where c.LlibreId = p.LLibreId and c.TempsId = p.TempsId
group by c.LlibreId, c.TempsId, p.UsuariId;