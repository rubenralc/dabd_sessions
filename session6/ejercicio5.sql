create materialized view log on employee
with rowid, sequence(employee_id)
including new values;

CREATE MATERIALIZED VIEW mv_assertion
BUILD IMMEDIATE REFRESH force ON commit AS
select department_id, email, count(*) as num_emp from employee
group by department_id, email;

ALTER TABLE mv_assertion ADD CONSTRAINT mv_check CHECK (num_emp < 2);
