select distinct e.nom_empl, d.edifici from EMPLEATS e, DEPARTAMENTS d
where e.num_dpt = '5' and e.num_dpt = d.num_dpt
order by e.nom_empl;

select distinct e.nom_empl, e.sou from EMPLEATS e
where e.num_dpt = '1' or e.num_dpt = '2'
order by e.nom_empl, e.sou;

select distinct d.num_dpt, d.nom_dpt from departaments d
join empleats e on  e.num_dpt = d.num_dpt
group by e.ciutat_empl, d.num_dpt, d.nom_dpt
having count(*) >= 2;

select d.num_dpt, d.nom_dpt from departaments d
where d.num_dpt not in (select e.num_dpt from empleats e where e.ciutat_empl = 'MADRID');