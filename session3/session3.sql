--Question 1
 CREATE TABLE Grup (
 nom varchar(1) ,
 monitor varchar(1) ,
 CONSTRAINT Grup_PK PRIMARY KEY (nom));
 
 CREATE TABLE Noi (
 Grup_nom varchar(1)  NOT NULL,
 nom varchar(1) ,
 edat varchar(1) ,
 CONSTRAINT Noi_FK_Grup FOREIGN KEY (Grup_nom) REFERENCES Grup(nom),
 CONSTRAINT Noi_PK PRIMARY KEY (nom));
 
 CREATE TABLE Mascota (
 Noi_nom varchar(1)  NOT NULL,
 nom varchar(1) ,
 CONSTRAINT Mascota_FK_Noi FOREIGN KEY (Noi_nom) REFERENCES Noi(nom),
 CONSTRAINT Mascota_PK PRIMARY KEY (nom));
 
 CREATE TABLE Amics (
 Noi1_nom varchar(1) NOT NULL,
 Noi2_nom varchar(1) NOT NULL,
 CONSTRAINT Amics1_FK_Noi FOREIGN KEY (Noi1_nom) REFERENCES Noi(nom),
 CONSTRAINT Amics2_FK_Noi FOREIGN KEY (Noi2_nom) REFERENCES Noi(nom),
 CONSTRAINT Amics_PK PRIMARY KEY (Noi1_nom, Noi2_nom));
 
 CREATE TABLE Incompatibles (
 Mascota1_nom varchar(1) NOT NULL,
 Mascota2_nom varchar(1) NOT NULL,
 CONSTRAINT Incompatibles1_FK_Mascota FOREIGN KEY (Mascota1_nom) REFERENCES Mascota(nom),
 CONSTRAINT Incompatibles2_FK_Mascota FOREIGN KEY (Mascota2_nom) REFERENCES Mascota(nom),
 CONSTRAINT Incompatibles_PK PRIMARY KEY (Mascota1_nom, Mascota2_nom));
 
--Question 2
 CREATE TABLE Esplai (
 Nom VARCHAR(1) ,
 Adreca VARCHAR(1) ,
 CONSTRAINT Esplai_PK PRIMARY KEY (Nom));
 
 CREATE TABLE Jornada (
 Data VARCHAR(1) ,
 SortSol VARCHAR(1) ,
 PostaSol VARCHAR(1) ,
 CONSTRAINT Jornada_PK PRIMARY KEY (Data));
 
 CREATE TABLE Noi (
 Nom VARCHAR(1) ,
 Cognom1 VARCHAR(1) ,
 Cognom2 VARCHAR(1) ,
 Edat VARCHAR(1) ,
 CONSTRAINT Noi_PK PRIMARY KEY (Cognom2,  Nom,  Cognom1));
 
 CREATE TABLE Tanda (
 Esplai_Nom VARCHAR(1)  NOT NULL,
 Ident VARCHAR(1) ,
 CONSTRAINT Tanda_FK_Esplai FOREIGN KEY (Esplai_Nom) REFERENCES Esplai(Nom),
 CONSTRAINT Tanda_PK PRIMARY KEY (Ident));
 
 CREATE TABLE Sortida (
 Tanda_Ident VARCHAR(1) NOT NULL,
 Jornada_Data VARCHAR(1) NOT NULL,
 Noi_Cognom2 VARCHAR(1) NOT NULL,
 Noi_Nom VARCHAR(1) NOT NULL,
 Noi_Cognom1 VARCHAR(1) NOT NULL,
 CONSTRAINT Sortida_FK_Tanda FOREIGN KEY (Tanda_Ident) REFERENCES Tanda(Ident),
 CONSTRAINT Sortida_FK_Jornada FOREIGN KEY (Jornada_Data) REFERENCES Jornada(Data),
 CONSTRAINT Sortida_FK_Noi FOREIGN KEY (Noi_Cognom2, Noi_Nom, Noi_Cognom1) REFERENCES Noi(Cognom2, Nom, Cognom1),
 CONSTRAINT Sortida_PK PRIMARY KEY (Jornada_Data, Noi_Cognom2, Noi_Nom, Noi_Cognom1));
 
 CREATE TABLE Calendari (
 Jornada_Data VARCHAR(1) NOT NULL,
 Tanda_Ident VARCHAR(1) NOT NULL,
 CONSTRAINT Calendari_FK_Jornada FOREIGN KEY (Jornada_Data) REFERENCES Jornada(Data),
 CONSTRAINT Calendari_FK_Tanda FOREIGN KEY (Tanda_Ident) REFERENCES Tanda(Ident),
 CONSTRAINT Calendari_PK PRIMARY KEY (Jornada_Data, Tanda_Ident));
 
 CREATE TABLE Membre (
 Noi_Cognom2 VARCHAR(1) NOT NULL,
 Noi_Nom VARCHAR(1) NOT NULL,
 Noi_Cognom1 VARCHAR(1) NOT NULL,
 Tanda_Ident VARCHAR(1) NOT NULL,
 CONSTRAINT Membre_FK_Noi FOREIGN KEY (Noi_Cognom2, Noi_Nom, Noi_Cognom1) REFERENCES Noi(Cognom2, Nom, Cognom1),
 CONSTRAINT Membre_FK_Tanda FOREIGN KEY (Tanda_Ident) REFERENCES Tanda(Ident),
 CONSTRAINT Membre_PK PRIMARY KEY (Noi_Cognom2, Noi_Nom, Noi_Cognom1, Tanda_Ident));
 
--Question 3
 CREATE TABLE Butlleti (
 DataCreacio VARCHAR(1) ,
 Titol VARCHAR(1) ,
 CONSTRAINT Butlleti_PK PRIMARY KEY (Titol));
 
 CREATE TABLE Federacio (
 Butlleti_Titol VARCHAR(1) ,
 Nom VARCHAR(1) ,
 DataFund VARCHAR(1) ,
 Zona VARCHAR(1) ,
 CONSTRAINT Federacio_FK_Butlleti FOREIGN KEY (Butlleti_Titol) REFERENCES Butlleti(Titol),
 CONSTRAINT Federacio_PK PRIMARY KEY (Nom));
 
 CREATE TABLE Esplai (
 Federacio_Nom VARCHAR(1) ,
 Butlleti_Titol VARCHAR(1) ,
 Nom VARCHAR(1) ,
 DataFund VARCHAR(1) ,
 Zona VARCHAR(1) ,
 Membres VARCHAR(1) ,
 CONSTRAINT Esplai_FK_Federacio FOREIGN KEY (Federacio_Nom) REFERENCES Federacio(Nom),
 CONSTRAINT Esplai_FK_Butlleti FOREIGN KEY (Butlleti_Titol) REFERENCES Butlleti(Titol),
 CONSTRAINT Esplai_PK PRIMARY KEY (Nom));
 
--Question 4
 CREATE TABLE Esplai (
 EdatMax VARCHAR(1) ,
 Nom VARCHAR(1) ,
 CONSTRAINT Esplai_PK PRIMARY KEY (Nom));
 
 CREATE TABLE EsplParv (
 Esplai_Nom VARCHAR(1) ,
 CONSTRAINT EsplParv_PK PRIMARY KEY (Esplai_Nom));
 
 CREATE TABLE EsplMobRed (
 Esplai_Nom VARCHAR(1) ,
 CONSTRAINT EsplMobRed_PK PRIMARY KEY (Esplai_Nom));
 
 CREATE TABLE Noi (
 Edat VARCHAR(1) ,
 Esplai_Nom VARCHAR(1) ,
 TipusDisc VARCHAR(1) ,
 Nom VARCHAR(1) ,
 CONSTRAINT Noi_FK_Esplai FOREIGN KEY (Esplai_Nom) REFERENCES Esplai(Nom),
 CONSTRAINT Noi_PK PRIMARY KEY (Nom));
 
 CREATE TABLE MobRed (
 EsplMobRed_Esplai_Nom VARCHAR(1)  NOT NULL,
 Noi_Nom VARCHAR(1) ,
 CONSTRAINT MobRed_FK_EsplMobRed FOREIGN KEY (EsplMobRed_Esplai_Nom) REFERENCES EsplMobRed(Esplai_Nom),
 CONSTRAINT MobRed_PK PRIMARY KEY (Noi_Nom));
 
 CREATE TABLE Parv (
 EsplParv_Esplai_Nom VARCHAR(1)  NOT NULL,
 Noi_Nom VARCHAR(1) ,
 CONSTRAINT Parv_FK_EsplParv FOREIGN KEY (EsplParv_Esplai_Nom) REFERENCES EsplParv(Esplai_Nom),
 CONSTRAINT Parv_PK PRIMARY KEY (Noi_Nom));
 
--Question 5
 CREATE TABLE Propietari (
 id VARCHAR(1) ,
 nom VARCHAR(1) ,
 adreca VARCHAR(1) ,
 edat VARCHAR(1) ,
 CONSTRAINT Propietari_PK PRIMARY KEY (id));
 
 CREATE TABLE Bicicleta (
 id VARCHAR(1) ,
 CONSTRAINT Bicicleta_PK PRIMARY KEY (id));
 
 CREATE TABLE possessio (
 proporcio VARCHAR(1),
 Propietari_id VARCHAR(1) NOT NULL,
 Bicicleta_id VARCHAR(1) NOT NULL,
 CONSTRAINT possessio_FK_Propietari FOREIGN KEY (Propietari_id) REFERENCES Propietari(id),
 CONSTRAINT possessio_FK_Bicicleta FOREIGN KEY (Bicicleta_id) REFERENCES Bicicleta(id),
 CONSTRAINT possessio_PK PRIMARY KEY (Propietari_id, Bicicleta_id));
 
 CREATE TABLE confianca (
 Propietari1_id VARCHAR(1) NOT NULL,
 Propietari2_id VARCHAR(1) NOT NULL,
 CONSTRAINT confianca1_FK_Propietari FOREIGN KEY (Propietari1_id) REFERENCES Propietari(id),
 CONSTRAINT confianca2_FK_Propietari FOREIGN KEY (Propietari2_id) REFERENCES Propietari(id),
 CONSTRAINT confianca_PK PRIMARY KEY (Propietari1_id, Propietari2_id));
 
--Question 6
 CREATE TABLE Corredor (
 id VARCHAR(1) ,
 nom VARCHAR(1) ,
 nacionalitat VARCHAR(1) ,
 edat VARCHAR(1) ,
 CONSTRAINT Corredor_PK PRIMARY KEY (id));
 
 CREATE TABLE Bicicleta (
 Corredor_id VARCHAR(1)  NOT NULL,
 id VARCHAR(1) ,
 CONSTRAINT Bicicleta_FK_Corredor FOREIGN KEY (Corredor_id) REFERENCES Corredor(id),
 CONSTRAINT Bicicleta_PK PRIMARY KEY (id));
 
 CREATE TABLE Carrera (
 id VARCHAR(1) ,
 CONSTRAINT Carrera_PK PRIMARY KEY (id));
 
 CREATE TABLE usa (
 Corredor_id VARCHAR(1) NOT NULL,
 Bicicleta_id VARCHAR(1) NOT NULL,
 Carrera_id VARCHAR(1) NOT NULL,
 CONSTRAINT usa_CK1 UNIQUE (Corredor_id, Carrera_id),
 CONSTRAINT usa_FK_Corredor FOREIGN KEY (Corredor_id) REFERENCES Corredor(id),
 CONSTRAINT usa_FK_Bicicleta FOREIGN KEY (Bicicleta_id) REFERENCES Bicicleta(id),
 CONSTRAINT usa_FK_Carrera FOREIGN KEY (Carrera_id) REFERENCES Carrera(id),
 CONSTRAINT usa_PK PRIMARY KEY (Bicicleta_id, Carrera_id));
 
--Question 7
 CREATE TABLE Bicicleta (
 id VARCHAR(1) ,
 CONSTRAINT Bicicleta_PK PRIMARY KEY (id));
 
 CREATE TABLE Corredor (
 club VARCHAR(1) ,
 federacio VARCHAR(1) ,
 edat VARCHAR(1) ,
 pes VARCHAR(1) ,
 alt VARCHAR(1) ,
 id VARCHAR(1) ,
 CONSTRAINT Corredor_PK PRIMARY KEY (id));
 
 CREATE TABLE Usa (
 Corredor_id VARCHAR(1)  NOT NULL,
 Bicicleta_id VARCHAR(1)  NOT NULL,
 CONSTRAINT Usa_FK_Corredor FOREIGN KEY (Corredor_id) REFERENCES Corredor(id),
 CONSTRAINT Usa_FK_Bicicleta FOREIGN KEY (Bicicleta_id) REFERENCES Bicicleta(id),
 CONSTRAINT Usa_PK PRIMARY KEY (Bicicleta_id));
 
--Question 8
 CREATE TABLE Bicicleta (
 id VARCHAR(1) ,
 CONSTRAINT Bicicleta_PK PRIMARY KEY (id));
 
 CREATE TABLE M (
 club VARCHAR(1) ,
 federacio VARCHAR(1) ,
 Corredor_id VARCHAR(1) ,
 CONSTRAINT M_PK PRIMARY KEY (Corredor_id));
 
 CREATE TABLE C (
 edat VARCHAR(1) ,
 pes VARCHAR(1) ,
 alt VARCHAR(1) ,
 Corredor_id VARCHAR(1) ,
 CONSTRAINT C_PK PRIMARY KEY (Corredor_id));
 
 CREATE TABLE Usa (
 C_Corredor_id VARCHAR(1) NOT NULL,
 Bicicleta_id VARCHAR(1)  NOT NULL,
 CONSTRAINT Usa_FK_C FOREIGN KEY (C_Corredor_id) REFERENCES C(Corredor_id),
 CONSTRAINT Usa_FK_Bicicleta FOREIGN KEY (Bicicleta_id) REFERENCES Bicicleta(id),
 CONSTRAINT Usa_PK PRIMARY KEY (Bicicleta_id));