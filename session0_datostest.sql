CREATE TABLE DEPARTAMENTS
         (	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT));

CREATE TABLE PROJECTES
         (	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ));

CREATE TABLE EMPLEATS
         (	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ));



INSERT INTO  DEPARTAMENTS VALUES (3,'MARKETING',3,'RIOS ROSAS','MADRID');
INSERT INTO  DEPARTAMENTS VALUES (5,'MARKETING',3,'MUNTANER','BARCELONA');
INSERT INTO  DEPARTAMENTS VALUES (1,'MARKETING',3,'RIOS ROSAS','BARCELONA');
INSERT INTO  DEPARTAMENTS VALUES (2,'DIRECCIO',8,'RIOS ROSAS','MADRID');

INSERT INTO  PROJECTES VALUES (1,'IBDTEL','TELEVISIO',1000000);

INSERT INTO  EMPLEATS VALUES (1,'ROBERTO',25000,'ZAMORA',3,1);
INSERT INTO  EMPLEATS VALUES (2,'ROBERTO2',25000,'BARCELONA',3,null);
INSERT INTO  EMPLEATS VALUES (5,'EULALIA',150000,'BARCELONA',3,null);
INSERT INTO  EMPLEATS VALUES (3,'ROBERTO3',25000,'MADRID',3,null);
INSERT INTO  EMPLEATS VALUES (4,'MIQUEL',25000,'MADRID',2,null);
INSERT INTO  EMPLEATS VALUES (6,'EULALIA',25000,'MADRID',5,null);

COMMIT;
