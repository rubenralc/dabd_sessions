--Question 1
CREATE TABLE DEPARTAMENTS
         (	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT));

CREATE TABLE PROJECTES
         (	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ));

CREATE TABLE EMPLEATS
         (	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ));


INSERT INTO DEPARTAMENTS VALUES (2, NULL, NULL, NULL,'Barcelona');
INSERT INTO  EMPLEATS VALUES (3, NULL, NULL, 'Barcelona', 2, NULL);

commit;

--For each city in the database, how many employees work there. Order the result by city. 
--The output of the test you'll find in the attached file should be:

--CITY COUNT 
--______ ____
--Barcelona 1

SELECT d.ciutat_dpt AS ciutat, COUNT(e.num_empl)
FROM departaments d LEFT OUTER JOIN empleats e ON d.num_dpt = e.num_dpt
GROUP BY d.ciutat_dpt
HAVING d.ciutat_dpt IS NOT NULL
UNION
SELECT e.ciutat_empl AS ciutat, 0
FROM empleats e
WHERE e.ciutat_empl IS NOT NULL AND
      e.ciutat_empl NOT IN (SELECT d.ciutat_dpt
                            FROM departaments d
                            WHERE d.ciutat_dpt IS NOT NULL)
ORDER BY ciutat

--Question 2
CREATE TABLE DEPARTAMENTS
         (	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT));

CREATE TABLE PROJECTES
         (	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ));

CREATE TABLE EMPLEATS
         (	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ));
	
	
INSERT INTO  DEPARTAMENTS VALUES (2,'MARKETING',3,'RIOS ROSAS','ZAMORA');
INSERT INTO  EMPLEATS VALUES (3,'ROBERTO',25000,'ZAMORA',2, NULL);

--For each department, obtain the number of employees from the company that
--live in the city where the department is. Order the result by the number of department. 
--The output of the test you'll find in the attached file should be:

--NUM_DPT COUNT 
--_________ ____ 
--2 1 

select d.num_dpt, count(e.num_empl) as Count from departaments d
left outer join empleats e on e.ciutat_empl = d.ciutat_dpt
group by d.num_dpt
order by d.num_dpt;

--Question 3
CREATE TABLE DEPARTAMENTS
         (	NUM_DPT INTEGER,
	NOM_DPT CHAR(20),
	PLANTA INTEGER,
	EDIFICI CHAR(30),
	CIUTAT_DPT CHAR(20),
	PRIMARY KEY (NUM_DPT));

CREATE TABLE PROJECTES
         (	NUM_PROJ INTEGER,
	NOM_PROJ CHAR(10),
	PRODUCTE CHAR(20),
	PRESSUPOST INTEGER,
	PRIMARY KEY (NUM_PROJ));

CREATE TABLE EMPLEATS
         (	NUM_EMPL INTEGER,
	NOM_EMPL CHAR(30),
	SOU INTEGER,
	CIUTAT_EMPL CHAR(20),
	NUM_DPT INTEGER,
	NUM_PROJ INTEGER,
	PRIMARY KEY (NUM_EMPL),
	FOREIGN KEY (NUM_DPT) REFERENCES DEPARTAMENTS (NUM_DPT),
	FOREIGN KEY (NUM_PROJ) REFERENCES PROJECTES (NUM_PROJ));
	
INSERT INTO  PROJECTES VALUES (1,'IBDVID','VIDEO',NULL);
INSERT INTO  EMPLEATS VALUES (3,'ROBERTO',25000,'ZAMORA',NULL,1);
INSERT INTO  EMPLEATS VALUES (6,'MARTA',15000,'ZAMORA',NULL,1);

--For each project that has the value null in the column "budget" (pressupost),
--obtain the sum of the salaries (sou) from the employees assigned to that
--project. Order the result by number of project. 
--The output of the test you'll find in the attached file should be:

--NUM_PROJ SUM 
--_________ ____ 
--1 40000 

select p.num_proj, sum(e.sou) as sum from projectes p
left outer join empleats e on p.num_proj = e.num_proj
where p.pressupost is null
group by p.num_proj
order by p.num_proj;


--Question 4
CREATE TABLE vigilants(
nom VARCHAR(20) PRIMARY key,
edat integer);

CREATE TABLE rondes(
hora INTEGER,
planta INTEGER,
vigilant VARCHAR(20) REFERENCES vigilants,
PRIMARY KEY(hora, planta));

CREATE TABLE habitacions(
num INTEGER,
planta INTEGER,
places INTEGER,
hora INTEGER,
minut INTEGER,
PRIMARY KEY(num, planta),
FOREIGN KEY(hora, planta) REFERENCES rondes);

INSERT INTO vigilants(nom, edat) VALUES ('Mulder', 32);
INSERT INTO vigilants(nom, edat) VALUES ('Scully', 30);

INSERT INTO rondes(hora, planta, vigilant) VALUES (7, 1, 'Mulder');
INSERT INTO rondes(hora, planta, vigilant) VALUES (8, 1, 'Mulder');
INSERT INTO rondes(hora, planta, vigilant) VALUES (7, 2, 'Mulder');

INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (1, 1, 1, 7, 30);
INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (5, 1, 1, 7, 30);
INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (2, 1, 1, 8, 30);
INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (3, 1, 1, null, null);
INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (4, 1, 1, null, null);
INSERT INTO habitacions(num, planta, places, hora, minut) VALUES (1, 2, 1, null, null);

commit;

--The table "habitacions" records, for each room, the time the guests must be
--awoken at (columns "hora" and "minut"). If the time is a null value, the
--guests must not be awoken. Give a SQL sentence returning the number of
--different hours (ignoring column "minut") someone must be awoken at.
--Willing not to be awoken is considered as a new different hour.

--The output of the test you'll find in the attached file should be:

--3

SELECT COUNT(*)
FROM habitacions h
WHERE h.num = (SELECT MAX(h2.num)
                FROM habitacions h2
                WHERE (h2.hora IS NULL AND h.hora IS NULL)
                    OR h2.hora = h.hora)
  AND h.planta = (SELECT MAX(h2.planta)
                  FROM habitacions h2
                  WHERE h.num = h2.num
                   AND ((h2.hora IS NULL AND h.hora IS NULL)
                      OR h2.hora = h.hora))
