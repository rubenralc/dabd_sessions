--Question 1
select distinct e1.nom_empl from empleats e1
where e1.sou = (select max(e2.sou) from empleats e2);

--Question 2
select d.num_dpt, d.nom_dpt from departaments d
join empleats e on e.num_dpt = d.num_dpt
group by d.num_dpt, d.nom_dpt
having count(distinct e.ciutat_empl) >= 2;

--Question 3
select d.num_dpt, d.nom_dpt, d.ciutat_dpt from departaments d
join empleats e on e.num_dpt = d.num_dpt
group by d.num_dpt, d.nom_dpt, d.ciutat_dpt
having count(distinct e.num_proj) = 1
order by d.num_dpt, d.nom_dpt, d.ciutat_dpt;

--Question 4
select d.modul, d.numero, d.superficie
from despatxos d
where d.superficie > (select avg(d2.superficie)
                      from despatxos d2
                      where exists (select *
                                    from assignacions a join professors p on a.dni = p.dni
                                    join centres c on p.codi_centre = c.codi_centre
                                    where a.modul = d2.modul and
                                    a.numero = d2.numero and
                                    c.nom_centre = 'FIB')
                      )
order by d.superficie;

--Question 5
select p.dni, p.nom_prof from professors p, centres c
where p.dni > c.codi_centre
group by p.dni, p.nom_prof
having count(*) >= 2
order by p.dni;

--Question 6
select distinct p.nom_prof, p.telefon from professors p
join assignacions a on p.dni = a.dni
where (a.modul = 'C5' or a.modul = 'C6') and to_char(a.data_inici_assig, 'YYYY') = '1997'
order by p.nom_prof;

--Question 7
select d.modul, d.numero, d.superficie from despatxos d
join assignacions a on d.modul = a.modul and d.numero = a.numero
where d.superficie > 10 and a.data_fi_assig is null
order by d.modul desc, d.numero desc;

--Question 8
select p.nom_prof from professors p
join assignacions a on p.dni = a.dni
join despatxos d on d.modul = a.modul and d.numero = a.numero
where d.superficie >= 15
order by p.nom_prof;

--Question 9
select t.especie from troballes t
where t.especie in (select especie from bolets where comestible = 'S')
group by t.especie
having count(distinct t.nom) > 2
order by t.especie asc;

--Question 10
select t.especie, b.valor, sum(t.quantitat) as total
from troballes t left join bolets b on b.especie = t.especie
where b.comestible = 'S'
group by t.especie, b.valor
having sum(t.quantitat)> 20;